"use strict";

function $(param) {   
    let p = param.match(/(#?)(.+)/);
    if (p[1]) return document.getElementById(p[2]);
    return document.getElementsByTagName(p[2]);
}

$.each = function(o, callback) {
    Array.prototype.forEach.call(o, callback);
}
