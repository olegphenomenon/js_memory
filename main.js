/* Oleg Hasjanov 2018 */

"use strict";

// Переопределяем протатип функции для перемешивания
Array.prototype.shuffle = function() {
    if (this.length == 1) return this;
    for (var j, x, i = this.length; i; j = Math.floor(Math.random() * i), x = this[--i], this[i] = this[j], this[j] = x);
    return this;
}    

// Загружаем карты
let dech = ['1.jpg', '2.jpg', '3.jpg', '4.jpg', '5.jpg', '6.jpg', '7.jpg', '8.jpg', '9.jpg', 
            '10.jpg', '11.jpg', '12.jpg', '13.jpg', '14.jpg', '15.jpg', '16.jpg', '17.jpg',
            '17.jpg', '18.jpg', '19.jpg', '20.jpg', ]

// объявляем глобальные переменные
let timeCardsShow = 5, // время показа карт лицовой стороной
    timeSameCardsShow = 3, // время показа двух выбранных карт
    totalCountCards = 10, // всего карт на игровом поле
    wrongMove = 0, // неверных ходов
    arrayCurrentCards = [], // массив карт, которые присуствуют в игре
    btnNewGame = null, // id кнопки
    wrongMessage = null, // id количества неверных ходов
    gameBox = null, // id игрового поля
    backSideCard = 'cover.jpg', // обратная сторона карт
    backSideCardsArray = []; //массив карт с обратной стороной

let clickTimes = 0; // счетчик открытых карт
    
window.onload = function() {
    gameBox = $("#box");
    btnNewGame = $("#new");
    btnNewGame.addEventListener('click', newGame);
}

// показать обратную сторону карт
function showBackGameCards() {
    gameBox.innerHTML = "";
    for(var i = 0; i < totalCountCards; i++)
    {
        var img = document.createElement('img');
        img.style = "width: 100px; height: 200px;"
        img.src = 'images/' + backSideCard;
        img.alt = i;
        img.addEventListener('click', showFaceCard);
        gameBox.appendChild(img);
        
    }
}

// показать карты игровой стороной
function showGameCards() { 
    // Размещаем карты на игровом поле и отображаем
    // TODO: НЕОБХОДИМ РЕФАКТОРИНГ! СДЕЛАТЬ ПО КРАСИВЕЕ!
    gameBox.innerHTML = "";
    arrayCurrentCards.shuffle(); // --> фасуем карты
    for(let j = 0; j < arrayCurrentCards.length; j++)
    {
        var img = document.createElement('img');
        img.style = "width: 100px; height: 200px;"
        img.src = 'images/' + arrayCurrentCards[j];
        gameBox.appendChild(img);
        
    }
}

// Функция новой игры. Запускается при нажании на кнопку "новая игра"
function newGame(dech_cards) {
    wrongMove = 0;
    $('#wrong').innerHTML = wrongMove;
    // Загружаем в случайном порядке 10 карт, которые отображатся на игровом поле в массив
    dech.shuffle();
    let arrayCards = []; //каждый раз переобределяем новый массив, 
                        // чтобы не делать изменений в константе карт
    // копируем содержимое, иначе в массиве будет не значения, а ссылки
    for (let i = 0; i < dech.length; i++) arrayCards[i] = dech[i]; 
    let i = 0;
    while(i < 10) {
        arrayCurrentCards[i] = arrayCards.shift();
        arrayCurrentCards[i+1] = arrayCurrentCards[i];
        i = i + 2;
    }
    setTimeout(showBackGameCards, timeCardsShow*1000);
    showGameCards();
}


let temps = [null, null]; // тут хроню временные объекты на которые был вызвать click
let timer; // идентификатор таймера
function showFaceCard(e) {
    clickTimes++; // контролер кликов
    if (clickTimes > 2) { clickTimes--; return; } // если количество кликов переваливает за 2, то уменьшить
    let idCard = e.target.alt; // по атрибуту alt обозначаю индефикатор

    clearTimeout(timer); // очищаю таймер, если вновь вызываю событие

    // если открыто меньше двух карт, то
    if (clickTimes <= 2) {
    e.target.src = 'images/' + arrayCurrentCards[idCard]; // показываю лицевую сторону карты
    temps[clickTimes-1] = e.target; // присваиваю во временную переменную объект вызыва


    if (clickTimes == 2) { // если открыто два объекта и произведенно больше двух кликов
        // устанавливаю таймер что б показать карты, а после закрываю их опять обратной стороной

        if (temps[0] === temps[1]) { clickTimes--; return;} // чтобы "не зависло" если счелкать только по
                                                        // одной карточке

        timer = setTimeout(function() {  
        if (temps[0].src != temps[1].src) {
            temps[0].src = 'images/' + backSideCard;
            temps[1].src = 'images/' + backSideCard;
            clickTimes = 0;
            wrongMove++;
            $('#wrong').innerHTML = wrongMove;
            return;
            }    
        }, timeSameCardsShow*1000);
        
        // если карты совпадают, то обнуляю счетчик кликов и убераю с них события
        if (temps[0].src === temps[1].src && temps[0].alt != temps[1].alt) {
        temps[0].removeEventListener('click', showFaceCard);
        temps[1].removeEventListener('click', showFaceCard);
        clickTimes = 0;
        return; 
        }      

    }
}
}